module AbstractFactory
  def create
    raise NotImplementedError, 'You should implement create method'
  end
end
