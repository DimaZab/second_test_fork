require 'singleton'
require 'mongo'

class DbConnector
  include Singleton

  attr_reader :client, :mongo_sequence

  def initialize
    @client = Mongo::Client.new(['127.0.0.1:27017'], database: 'test_app')
  end
end
