require_relative './db_connector'

class ActiveRecord

  CONNECTOR = DbConnector.instance

  attr_reader :attributes

  def initialize(attributes = {})
    set(attributes)
  end

  class << self
    def find(expression)
      self.new.set(CONNECTOR.client[model_name].find(expression).limit(1).first)
    end

    def model_name
      self.name.downcase
    end
  end

  def set(attributes)
    @attributes = attributes
    self
  end

  def save
    CONNECTOR.client[self.class.model_name].insert_one(attributes).successful?
  end

  def get
    @attributes
  end
end
