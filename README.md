# Тестовое задание #

* Установить MongoDB.

* Клонировать репозиторий git clone https://DimaZab@bitbucket.org/DimaZab/second_test_fork.git

* Заходим в папку и запускаем bundle.

```
#!console

cd second_test_fork/
bundle
```
* Запускаем код. По умолчанию путь к папке './data/'.


```
#!console

rake upload['path_to_dir_with_files/']
```
* Запускаем тесты.


```
#!console

rake test
```
