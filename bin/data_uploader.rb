require_relative 'factories/file_reader_factory.rb'
require_relative 'factories/model_factory.rb'

class DataUploader
  def initialize(file)
    @file = file
  end

  def upload
    file_reader.features.each do |feature|
      model = model_factory.create
      model.set(feature)
      model.save
    end
  end

  private

  attr_reader :file

  def file_reader
    file_reader_factory.create
  end

  def file_reader_factory
    @file_reader_factory ||= FileReaderFactory.new(file)
  end

  def model_factory
    @model_factory ||= ModelFactory.new(file)
  end
end
