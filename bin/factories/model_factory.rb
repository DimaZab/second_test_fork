require_relative '../../lib/abstract_factory.rb'
require_relative '../models/user.rb'
require_relative '../models/post.rb'

class ModelFactory
  include AbstractFactory

  def initialize(file)
    @file = file
  end

  def create
    model.new
  end

  private

  attr_reader :file

  def model
    @model ||= Object.const_get(model_name)
  end

  def model_name
    file_name.split('_').map(&:capitalize).join('')
  end

  def file_name
    File.basename(file, '.*')
  end
end
