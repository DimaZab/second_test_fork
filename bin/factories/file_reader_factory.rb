require_relative '../../lib/abstract_factory.rb'
require_relative '../file_readers/json_file_reader.rb'
require_relative '../file_readers/ini_file_reader.rb'

class FileReaderFactory
  include AbstractFactory

  def initialize(file)
    @file = file
  end

  def create
    file_reader.new(file)
  end

  private

  attr_reader :file

  def file_reader
    @file_reader ||= Object.const_get(file_reader_name)
  end

  def file_reader_name
    file_ext.delete('.').split('_').map(&:capitalize).join('') + 'FileReader'
  end

  def file_ext
    File.extname(file)
  end
end
