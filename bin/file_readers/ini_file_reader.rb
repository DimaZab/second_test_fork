require_relative './file_reader.rb'

class IniFileReader < FileReader
  def features
    group_by_model_alias.map do |_, line|
      record_hash(line)
    end
  end

  private

  def record_hash(line)
    line.map do |_, key, value|
      [key, value]
    end.to_h
  end

  def scaned_file
    readed_file.scan(/\[(.*?)\]\[(.*?)\]..."(.*?)"/)
  end

  def group_by_model_alias
    scaned_file.group_by(&:first)
  end
end
