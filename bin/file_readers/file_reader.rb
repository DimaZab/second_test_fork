class FileReader
  def initialize(file)
    @file = file
  end

  def features
    raise NotImplementedError, 'You should implement features method'
  end

  private

  attr_reader :file

  def readed_file
    File.read(file)
  end
end
