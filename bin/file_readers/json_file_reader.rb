require_relative './file_reader.rb'
require 'json'

class JsonFileReader < FileReader
  def features
    JSON.parse(readed_file)
  end
end
