module ModelTestHelper
  def attributes
    raise NotImplementedError, 'You should implement attributes method'
  end

  def model_class
    raise NotImplementedError, 'You should implement model_class method'
  end

  def expression
    raise NotImplementedError, 'You should implement expression method'
  end

  def setup
    model_class.new(attributes).save
  end

  def teardown
    DbConnector.instance.client[:post].drop
  end

  def test_save
    assert model_class.new(attributes).save
  end

  def test_set_get
    assert_equal(attributes, model_class.new(attributes).get)
  end

  def test_find
    assert_equal(attributes[:text], model_class.find(expression).get['text'])
  end
end
