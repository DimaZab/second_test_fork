require 'test/unit'
require_relative '../../bin/models/user.rb'
require_relative '../../lib/db_connector.rb'
require_relative './model_test_helper.rb'

class UserTest < Test::Unit::TestCase
  include ModelTestHelper

  def attributes
    { name: 'first_test_user', text: "I'm a first_test_user text" }
  end

  def model_class
    User
  end

  def expression
    { name: 'first_test_user' }
  end
end
