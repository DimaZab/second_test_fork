require 'test/unit'
require_relative '../../bin/models/post.rb'
require_relative '../../lib/db_connector.rb'
require_relative './model_test_helper.rb'

class PostTest < Test::Unit::TestCase
  include ModelTestHelper

  def attributes
    { name: 'first_test_post', text: "I'm a first_test_post text" }
  end

  def model_class
    Post
  end

  def expression
    { name: 'first_test_post' }
  end
end
