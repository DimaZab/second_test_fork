require 'test/unit'
require_relative '../../bin/file_readers/json_file_reader.rb'
require_relative './file_reader_test_helper.rb'

class IniFileReaderTest < Test::Unit::TestCase
  include FileReaderTestHelper

  def first
    { 'name' => 'Dima', 'last_name' => 'Zabolotnyi', 'age' => 22 }
  end

  def file_reader_class
    JsonFileReader
  end

  def file_path
    '/data/user.json'
  end
end
