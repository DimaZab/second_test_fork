module FileReaderTestHelper
  attr_reader :file_reader

  def file_reader_class
    raise NotImplementedError, 'You should implement file_reader_class method'
  end

  def file_path
    raise NotImplementedError, 'You should implement file_path method'
  end

  def first
    raise NotImplementedError, 'You should implement first method'
  end

  def setup
    @file_reader = file_reader_class.new(file)
  end

  def teardown
    @file_reader = nil
  end

  def test_features_first
    assert_equal(first, file_reader.features.first)
  end

  def file
    @file = File.new(Dir.pwd + file_path, 'r')
  end

  def test_features_count
    assert_equal(2, file_reader.features.size)
  end
end
