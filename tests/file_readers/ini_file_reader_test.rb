require 'test/unit'
require_relative '../../bin/file_readers/ini_file_reader.rb'
require_relative './file_reader_test_helper.rb'

class IniFileReaderTest < Test::Unit::TestCase
  include FileReaderTestHelper

  def first
    { 'name' => 'Vova', 'last_name' => 'Goisan', 'age' => '21' }
  end

  def file_reader_class
    IniFileReader
  end

  def file_path
    '/data/user.ini'
  end
end
