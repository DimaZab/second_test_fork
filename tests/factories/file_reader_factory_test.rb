require 'test/unit'
require_relative '../../bin/factories/file_reader_factory.rb'
require_relative './factory_test_helper.rb'

class FileReaderFactoryTest < Test::Unit::TestCase
  include FactoryTestHelper

  def class_name(type)
    type.capitalize + 'FileReader'
  end

  def file_name(type)
    'user.' + type
  end

  def types
    %w(ini json)
  end

  def factory_class
    FileReaderFactory
  end
end
