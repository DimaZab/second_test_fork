require 'test/unit'
require_relative '../../bin/factories/model_factory.rb'
require_relative './factory_test_helper.rb'

class ModelFactoryTest < Test::Unit::TestCase
  include FactoryTestHelper

  def class_name(type)
    type.capitalize
  end

  def file_name(name)
    name + '.json'
  end

  def types
    %w(user post)
  end

  def factory_class
    ModelFactory
  end
end
