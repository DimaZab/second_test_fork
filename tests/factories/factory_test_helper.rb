module FactoryTestHelper
  def test_factory
    types.each do |type|
      factory = factory_class.new file(type)
      assert_equal(class_name(type), factory.create.class.name)
    end
  end

  def file(type)
    File.new(Dir.pwd + '/data/' + file_name(type), 'r')
  end

  def class_name(type)
    raise NotImplementedError, 'You should implement class_name method'
  end

  def file_name(type)
    raise NotImplementedError, 'You should implement file_name method'
  end

  def types
    raise NotImplementedError, 'You should implement types method'
  end

  def factory_class
    raise NotImplementedError, 'You should implement factory_class method'
  end
end
